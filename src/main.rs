/*
    SPDX-FileCopyrightText: 2022 Nicolas Fella <nicolas.fella@gmx.de>

    SPDX-License-Identifier: MIT
*/

use serde::Deserialize;
use serde::Serialize;
use serde_yaml::Value;
use std::collections::BTreeMap as Map;
use std::env;
use std::fs::File;
use std::io::Write;
use std::io::{self, BufRead};
use std::path::Path;
use tinytemplate::TinyTemplate;

#[derive(Deserialize)]
struct KDECI {
    #[serde(rename = "Dependencies")]
    dependencies: Option<Vec<Dependencies>>,
    #[serde(rename = "Options")]
    options: Option<Options>,
}

#[derive(Deserialize)]
struct Dependencies {
    require: Require,
}

#[derive(Deserialize)]
struct Require {
    #[serde(flatten)]
    other: Map<String, Value>,
}

#[derive(PartialEq, Eq)]
struct Project {
    base_name: String,
}

#[derive(Deserialize)]
struct Options {
    #[serde(rename = "require-passing-tests-on")]
    require_passing_tests_on: Option<Vec<String>>,
}

#[derive(Deserialize)]
struct GitlabCI {
    include: Vec<String>,
}

struct ProjectData {
    name: String,
    missingTestPlatforms: Vec<String>,
}

impl Project {
    fn from_full_name(full_name: String) -> Self {
        Project {
            base_name: full_name.split("/").into_iter().nth(1).unwrap().to_string(),
        }
    }

    fn from_base_name(base_name: String) -> Self {
        Project { base_name }
    }

    fn base_name(&self) -> String {
        self.base_name.to_string()
    }

    fn printable_name(&self) -> String {
        self.base_name().replace("-", "_")
    }

    fn display_name(&self) -> String {
        self.base_name.to_string()
    }
}

#[derive(Serialize)]
struct Context {
    projects: Vec<String>,
}

fn main() {
    let mut projects = load_project_data();

    projects.retain(|project| !project.missingTestPlatforms.is_empty());

    let template_file: String = env::var("CIDEPS_TEMPLATE_FILE").unwrap();

    let template: String = std::fs::read_to_string(template_file).unwrap();

    let project_display_strings = projects
        .iter()
        .map(|project| {
            format!(
                "{}: {}",
                project.name,
                project.missingTestPlatforms.join(" ")
            )
        })
        .collect();

    let mut tt = TinyTemplate::new();
    tt.add_template("the_template", &template).unwrap();

    let context = Context { projects: project_display_strings };

    let rendered = tt.render("the_template", &context);

    let ouput_file: String = env::var("CITESTS_OUTPUT_FILE").unwrap();

    let mut output_file = File::create(ouput_file).unwrap();

    output_file.write_all(rendered.unwrap().as_bytes()).unwrap();
}

fn load_project_data() -> Vec<ProjectData> {
    let mut result = vec![];

    let projects_home: String = env::var("CIDEPS_PROJECT_HOME").unwrap();

    let projects = list_projects();

    for project in projects {

        let maybe_kdeci_file = std::fs::File::open(format!(
            "{}/{}/.kde-ci.yml",
            projects_home,
            project.base_name()
        ));

        let maybe_gitlabci_file = std::fs::File::open(format!(
            "{}/{}/.gitlab-ci.yml",
            projects_home,
            project.base_name()
        ));

        if maybe_kdeci_file.is_err() || maybe_gitlabci_file.is_err() {
            continue;
        }

        let kdeci: KDECI = serde_yaml::from_reader(maybe_kdeci_file.unwrap()).unwrap();
        let gitlabci: GitlabCI = serde_yaml::from_reader(maybe_gitlabci_file.unwrap()).unwrap();

        let required_tests_platforms = match kdeci.options {
            None => vec![],
            Some(options) => options.require_passing_tests_on.unwrap_or(vec![]),
        };

        let ci_platforms = ci_platforms(&gitlabci.include);

        let missing_platforms = missing_platforms(&ci_platforms, &required_tests_platforms);

        result.push(ProjectData {
            name: project.base_name(),
            missingTestPlatforms: missing_platforms,
        });
    }

    return result;
}

fn ci_platforms(includes: &Vec<String>) -> Vec<String> {
    let mut result = vec![];

    for line in includes {
        if line.ends_with("linux.yml") {
            result.push("Linux/Qt5".to_string());
        } else if line.ends_with("linux-qt6.yml") {
            result.push("Linux/Qt6".to_string());
        } else if line.ends_with("freebsd.yml") {
            result.push("FreeBSD/Qt5".to_string());
        } else if line.ends_with("freebsd-qt6.yml") {
            result.push("FreeBSD/Qt6".to_string());
        } else if line.ends_with("windows.yml") {
            result.push("Windows/Qt5".to_string());
        } else if line.ends_with("windows-qt6.yml") {
            result.push("Windows/Qt6".to_string());
        }
        // no Android since we don't run tests there'
    }

    return result;
}

fn missing_platforms(ci_platforms: &Vec<String>, tests_platforms: &Vec<String>) -> Vec<String> {
    let mut result = vec![];

    for ci_platform in ci_platforms {
        let found = tests_platforms.iter().find(|test_platform| {
            if test_platform == &ci_platform {
                return true;
            }

            if ci_platform.starts_with(&test_platform.to_string()) {
                return true;
            }

            return false;
        });

        if found.is_none() {
            result.push(ci_platform.to_string());
        }
    }

    return result;
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn list_projects() -> Vec<Project> {
    let mut result = vec![];

    let missing_file: String = env::var("CITESTS_PROJECTS_FILE").unwrap();

    if let Ok(lines) = read_lines(missing_file) {
        for line in lines {
            if let Ok(project_name) = line {
                result.push(Project::from_full_name(project_name));
            }
        }
    }

    return result;
}
